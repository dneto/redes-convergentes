package br.unifor.redesconvergentes.test;

import java.io.IOException;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.snmp4j.TransportMapping;

import br.unifor.redesconvergentes.snmp.SNMPConnection;
import br.unifor.redesconvergentes.snmp.SNMPException;
import br.unifor.redesconvergentes.snmp.config.SNMPConfig;

public class ConnectionTest {
	@Mock
	private TransportMapping transportMapping;

	@Before
	public void startUp() {
		MockitoAnnotations.initMocks(this);
	}

	@Test
	public void testSuccessListen() {
		try {
			Mockito.doNothing().when(transportMapping).listen();
			getConnection();
		} catch (SNMPException e) {
			Assert.assertTrue(false);
		} catch (IOException e) {
			Assert.assertTrue(false);
		}
	}

	@Test
	public void testSuccessGetMib() {
		try {
			String mibInfo = "1.2.3.4.5.6";
			SNMPConnection smnpConnection = getConnection();
			smnpConnection.getMIBInfo(mibInfo);
		} catch (SNMPException e) {
			Assert.assertTrue(false);
		}
	}

	private SNMPConnection getConnection() throws SNMPException {
		SNMPConnection snmpConnection = new SNMPConnection(new SNMPConfig(
				"127.0.0.1", 161));
		snmpConnection.listen();

		return snmpConnection;
	}

}
