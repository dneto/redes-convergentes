package br.unifor.redesconvergentes.test.config;

import java.io.IOException;
import java.util.Properties;

import org.apache.log4j.Logger;


public class TestConfig {
	public static String ADDRESS;
	public static Integer PORT;
	private static Logger LOGGER = Logger.getLogger(TestConfig.class);
	
	
	public static void init(){
		String address = null;
		Integer port = null;
		
		try {
			Properties p = new Properties();
			p.load(TestConfig.class.getClassLoader().getResourceAsStream(
					"ConnectionTest.properties"));
			port = (Integer) p.get("connection.port");
			address = (String) p.get("connection.address");
		} catch (IOException e) {
			LOGGER.fatal("Não foi possível");
		}
		
		ADDRESS = address;
		PORT = port;
	}
}
