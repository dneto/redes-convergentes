var snmpApp = angular.module('SNMPApp', [ 'ngRoute', 'ngResource' ]);

snmpApp.config([ '$routeProvider', function($routeProvider) {
	$routeProvider.when('/',{
		templateUrl : 'pages/index.html',
		controller : 'MainCtrl'
	})
	.when('/firstTime', {
		templateUrl : 'pages/firsttime.html',
		controller : 'FirstTimeController'
	})
	.when('/hosts',{
		templateUrl : 'pages/hosts.html',
		controller : 'SNMPConnectionController'
	})
	.otherwise({
		redirectTo : "/firstTime"
	});
} ]);

snmpApp.controller('MainCtrl',  ['$scope','$http','$window','$location', function($scope, $http, $window, $location) {
	$http.get('/host/list.json').success(function(data) {
		if(data.length > 0){
			$location.url("/hosts");
		}else{
			$location.url("/firstTime");
		}
	});
}]);

snmpApp.controller('FirstTimeController', ['$scope','$http','$window','$location', function($scope, $http, $window, $location) {
	$scope.save = function(snmp){
		$http.post('/host/add',snmp).success(function($snmp){
			if($snmp.address != undefined){
				$location.url("/");
			}
		});

	};
}]);

snmpApp.controller('SNMPConnectionController', function($scope, $http, $location) {
		$scope.addNew = false;
		
		var inactivateAll = function(){
			$scope.hosts.map(function(host){
				host.active = false;
				$scope.addNew = false;
			});
		};
		
		$scope.activeNew = function(){
			inactivateAll();
			$scope.addNew = true;
		};
		
		$scope.active = function(host){
			inactivateAll();
			host.active = true;
			$scope.addNew = false;
			
			$http.post('/mibs.json', host.address).success(function(mibs) {
				$scope.mibs = mibs;
			});
		};
		
		$http.get('/host/list.json').success(function(data) {
			$scope.hosts = data;
			inactivateAll();
			$scope.hosts[0].active = true;
			$scope.active($scope.hosts[0]);
		});
		
		
});