package br.unifor.redesconvergentes.snmp;

import java.io.IOException;

import org.apache.log4j.Logger;
import org.snmp4j.CommunityTarget;
import org.snmp4j.PDU;
import org.snmp4j.Snmp;
import org.snmp4j.Target;
import org.snmp4j.TransportMapping;
import org.snmp4j.event.ResponseEvent;
import org.snmp4j.mp.SnmpConstants;
import org.snmp4j.smi.GenericAddress;
import org.snmp4j.smi.OID;
import org.snmp4j.smi.OctetString;
import org.snmp4j.smi.VariableBinding;
import org.snmp4j.transport.DefaultUdpTransportMapping;

import br.unifor.redesconvergentes.snmp.config.SNMPConfig;

public class SNMPConnection {

	private static Logger LOGGER = Logger.getLogger(SNMPConnection.class);
	private int port;
	private String address;

	private Snmp snmp;
	private TransportMapping transportMapping;
	private SNMPConfig snmpConfig;

	public SNMPConnection(SNMPConfig snmpConfig) {
		this.snmpConfig = snmpConfig;
		this.port = snmpConfig.getPort();
		this.address = snmpConfig.getAddress();
	}

	public SNMPConnection(TransportMapping transportMapping) {
		super();
		this.transportMapping = transportMapping;
	}

	public void listen() throws SNMPException {
		try {
			transportMapping = new DefaultUdpTransportMapping();
			snmp = new Snmp(transportMapping);
			transportMapping.listen();
		} catch (IOException e) {
			e.printStackTrace();
		}

	}

	public String getMIBInfo(String mib) throws SNMPException {
		PDU pdu = new PDU();
		pdu.setType(PDU.GET);
		pdu.add(new VariableBinding(new OID(mib)));
		try {
			ResponseEvent ev = snmp.send(pdu, buildTarget());
			if (ev != null) {
				return ev.getResponse().get(0).getVariable().toString();
			}
		} catch (IOException e) {
			LOGGER.error("Não foi possível obter informação para a MIB: " + mib);
			throw new SNMPException(e.getMessage()
					+ " Não foi possível obter informação para a MIB: " + mib);
		}

		return null;
	}

	private Target buildTarget() {
		CommunityTarget communityTarget = new CommunityTarget();
		communityTarget
				.setCommunity(new OctetString(snmpConfig.getCommunity()));
		communityTarget.setAddress(GenericAddress.parse(snmpConfig.getAddress()
				+ "/" + snmpConfig.getPort()));
		communityTarget.setRetries(snmpConfig.getRetries());
		communityTarget.setTimeout(snmpConfig.getTimeout());
		communityTarget.setVersion(SnmpConstants.version2c);

		return communityTarget;
	}

	public int getPort() {
		return port;
	}

	public void setPort(int port) {
		this.port = port;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

}
