package br.unifor.redesconvergentes.snmp;

public enum MibEnum {
	SYS_INFO("1.3.6.1.2.1.1.1.0", "Informações do Sistema"),
	SYS_NAME("1.3.6.1.2.1.1.5.0","Host"),
	QT_SERV("1.3.6.1.2.1.1.7.0","Quantidade de Serviços"),
	SYS_UP_TIME("1.3.6.1.2.1.1.3.0", "Uptime"),
	QT_INTERFACE("1.3.6.1.2.1.2.1.0", "Qt. Intefaces"),
	DEVICE_MODEL("1.3.6.1.2.1.25.1.3.0","Modelo do dispositivo"),
    SYS_STORAGE("1.3.6.1.2.1.25.2.2.0","Quantidade de espaço da RAM"),
    PKG_IN("1.3.6.1.2.1.11.1.0","Pacotes IN SNMP"),
    PKG_OUT("1.3.6.1.2.1.11.2.0","Pacotes OUT SNMP"),
    INT_NAME("1.3.6.1.2.1.2.2.1.2.2","Nome da interface \"2\"");

	
	private String mib;
	private String name;
	
	MibEnum(String mib, String name) {
		this.mib = mib;
		this.name = name;
	}
	
	MibEnum(String mib){
		this.name = this.toString();
		this.mib = mib;
	}

	public String getMib() {
		return mib;
	}

	public void setMib(String mib) {
		this.mib = mib;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
	
	public Mib toMib(){
		Mib ret =  new Mib();
		ret.setOid(this.mib);
		ret.setName(this.name);
		return ret;
	}

}
