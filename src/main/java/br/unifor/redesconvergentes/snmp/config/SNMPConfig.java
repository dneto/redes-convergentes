package br.unifor.redesconvergentes.snmp.config;

import java.io.IOException;
import java.util.Properties;

public class SNMPConfig {

	private String address;
	private Integer port;
	private Integer retries;
	private Integer timeout;
	private String community;

	public SNMPConfig(String address, Integer port) {
		this();
		this.address = address;
		this.port = port;
	}

	public SNMPConfig() {
		Properties prop = new Properties();
		try {
			prop.load(Thread
					.currentThread()
					.getContextClassLoader()
					.getResourceAsStream(
							"br/unifor/redesconvergentes/snmp/config/SNMPConfig.properties"));
			this.retries = Integer.valueOf(prop.getProperty("snmp.retries"));
			this.timeout = Integer.valueOf(prop.getProperty("snmp.timeout"));
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public Integer getPort() {
		return port;
	}

	public void setPort(Integer port) {
		this.port = port;
	}

	public Integer getRetries() {
		return retries;
	}

	public void setRetries(Integer retries) {
		this.retries = retries;
	}

	public Integer getTimeout() {
		return timeout;
	}

	public void setTimeout(Integer timeout) {
		this.timeout = timeout;
	}

	public String getCommunity() {
		return community;
	}

	public void setCommunity(String community) {
		this.community = community;
	}
}
