package br.unifor.redesconvergentes.snmp;

public class SNMPException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public SNMPException() {
	}

	public SNMPException(String message) {
		super(message);
	}

}
