package br.unifor.redesconvergentes.web.data;

import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

import br.unifor.redesconvergentes.snmp.SNMPConnection;

public class InMemoryDatabase {
	private static InMemoryDatabase instance;

	public static InMemoryDatabase getInstance() {
		return instance == null ? new InMemoryDatabase() : instance;
	}

	private final static Map<String, SNMPConnection> database = new HashMap<>();

	private InMemoryDatabase() {
	}

	public void insert(String ip, SNMPConnection conn) {
		database.put(ip, conn);
	}

	public void delete(String ip) {
		database.remove(ip);
	}

	public SNMPConnection select(String ip) {
		return database.get(ip);
	}

	public Collection<String> selectList() {
		return database.keySet();
	}

}
