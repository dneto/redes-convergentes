package br.unifor.redesconvergentes.web.controller;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import br.unifor.redesconvergentes.snmp.SNMPConnection;
import br.unifor.redesconvergentes.snmp.config.SNMPConfig;
import br.unifor.redesconvergentes.web.data.InMemoryDatabase;

@Controller
@RequestMapping("/host")
public class SNMPConnController {
	
	private InMemoryDatabase db = InMemoryDatabase.getInstance();
	
	@RequestMapping("list.json")
	@ResponseBody
	public Collection<SNMPConfig> getHostList(){
		List<SNMPConfig> list = new ArrayList<>();
		for(String s:db.selectList()){
			SNMPConfig scon = new SNMPConfig();
			scon.setAddress(s);
			
			list.add(scon);
		}
		return list;
	}
	
	@RequestMapping(value="/add", method= RequestMethod.POST)
	@ResponseBody
	public SNMPConfig save(@RequestBody SNMPConfig snmp){
		String address = snmp.getAddress();
		SNMPConnection conn = new SNMPConnection(snmp);
		db.insert(address, conn);
		return snmp;
	}
	

}
