package br.unifor.redesconvergentes.web.controller;

import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import br.unifor.redesconvergentes.snmp.Mib;
import br.unifor.redesconvergentes.snmp.MibEnum;
import br.unifor.redesconvergentes.snmp.SNMPConnection;
import br.unifor.redesconvergentes.snmp.SNMPException;
import br.unifor.redesconvergentes.web.data.InMemoryDatabase;

@Controller
public class MibController {

	@RequestMapping("/mibs.json")
	@ResponseBody
	public Mib[] getMibInfo(@RequestBody String ipAddress) {

		SNMPConnection con = InMemoryDatabase.getInstance().select(ipAddress);
		List<Mib> mibs = new ArrayList<>();
		if (ipAddress != null && !ipAddress.isEmpty() && con != null) {
			try {
				con.listen();
				for (MibEnum mib : MibEnum.values()) {
					Mib m = mib.toMib();
					m.setValue(con.getMIBInfo(mib.getMib()));
					mibs.add(m);
				}
			} catch (SNMPException e) {
				e.printStackTrace();
			}
		}
		return mibs.toArray(new Mib[] {});
	}
}
